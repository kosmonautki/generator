package com.generatorGrupProjekt.generatorGrup.service;

import com.generatorGrupProjekt.generatorGrup.Repository.MemberJpaRepository;
import com.generatorGrupProjekt.generatorGrup.model.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemberService implements MemberServiceInterface {

    @Autowired
    private MemberJpaRepository memberJpaRepository;

    @Override
    public List<Member> getAllMembers() {
        return memberJpaRepository.findAll();
    }

    @Override
    public List<Member> getMemberByName(String name) {
        return memberJpaRepository.findByNameIgnoreCaseContaining(name);
    }

    @Override
    public List<Member> getMembersByExperience(int experience) {
        return memberJpaRepository.findByExperience(experience);
    }

    @Override
    public int getMaxValueExperience() {
        return memberJpaRepository.findAllByOrderByExperienceDesc().get(0).getExperience();
    }

    @Override
    public Member getById(Long id) {
        return memberJpaRepository.findById(id).get();
    }

    @Override
    public Member addMember(Member member) {
        return memberJpaRepository.save(member);
    }

    @Override
    public Member modifyMember(Member member) {
        return memberJpaRepository.save(member);
    }

    @Override
    public boolean deleteMember(Long id) {

        try {
            memberJpaRepository.deleteById(id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
