package com.generatorGrupProjekt.generatorGrup.service;

import com.generatorGrupProjekt.generatorGrup.model.Member;

import java.awt.*;
import java.util.List;

public interface MemberServiceInterface {

    List<Member> getAllMembers();

    List<Member> getMemberByName(String name);

    List<Member> getMembersByExperience(int experence);

    int getMaxValueExperience();

    Member getById(Long id);

    Member addMember(Member member);

    Member modifyMember(Member member);

    boolean deleteMember(Long id);


}
