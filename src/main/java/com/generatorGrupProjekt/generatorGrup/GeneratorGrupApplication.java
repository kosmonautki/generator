package com.generatorGrupProjekt.generatorGrup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeneratorGrupApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeneratorGrupApplication.class, args);
	}

}
