package com.generatorGrupProjekt.generatorGrup.Controller;

import com.generatorGrupProjekt.generatorGrup.model.Member;
import com.generatorGrupProjekt.generatorGrup.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.*;

@RestController
@RequestMapping("rest/member")
class MemberController {

    @Autowired
    private final MemberService memberService;


    public MemberController(MemberService memberService) {
        this.memberService = memberService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/save")
    private ResponseEntity<Member> save(@RequestBody MemberApi memberApi) {
        Member memberEntity = new Member();

        memberEntity.setId(memberApi.getId());
        memberEntity.setName(memberApi.getName());
        memberEntity.setExperience(memberApi.getExperience());

        Member memberUser = memberService.addMember(memberEntity);
        return ResponseEntity.ok(memberUser);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/all")
    public ResponseEntity<List<Member>> getAll() {
        return ResponseEntity.ok(memberService.getAllMembers());
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/{experience}")
    public ResponseEntity<List<Member>> getByExperienceDef(@PathVariable("experience") int experience) {
        return ResponseEntity.ok(memberService.getMembersByExperience(experience));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/group")
    public ResponseEntity<Map<Integer, List<Member>>> getByExperience() {
        Map<Integer, List<Member>> membersByExperienceMap = new HashMap<>();
        int maxExperience = memberService.getMaxValueExperience();
        Random random = new Random();

        Map<Integer, List<Member>> groups = new HashMap<>();

        // podział na tz. koszyki uwzgledniajace doświdczenie
        for (int i = maxExperience; i >= 0; i--) {
            List<Member> membersByExperience = memberService.getMembersByExperience(i);
            membersByExperienceMap.put(i, membersByExperience);
            System.out.println(membersByExperienceMap.get(i));
        }
        int basket = membersByExperienceMap.get(maxExperience).size();
        // losowanie grup


        for (int j = 0; j < membersByExperienceMap.size(); j++) {
            List<Member> list = membersByExperienceMap.get(j);
            while (list.size() > 0) {
                for (int i = 0; i < basket; i++) {
                    if (!groups.containsKey(i)) {
                        Member memberRandomed = list.get(random.nextInt(list.size()));
                        List<Member> groupList = new ArrayList<>();
                        groupList.add(memberRandomed);
                        groups.put(i, groupList);
                        list.remove(memberRandomed);

                    } else {
                        Member memberRandomed = list.get(random.nextInt(list.size()));
                        groups.get(i).add(memberRandomed);
                        list.remove(memberRandomed);

                        if (list.size() == 0) {
                            break;
                        }
                    }

                }

            }


        }


        return ResponseEntity.ok(groups);
    }

}
