package com.generatorGrupProjekt.generatorGrup.Repository;

import com.generatorGrupProjekt.generatorGrup.model.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MemberJpaRepository extends JpaRepository<Member, Long> {

    List<Member> findByNameIgnoreCaseContaining(String name);

    List<Member> findByExperience(int experience);

    List<Member> findAllByOrderByExperienceDesc();
}
